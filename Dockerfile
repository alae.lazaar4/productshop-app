FROM openjdk:8-jre-slim

# Set the working directory in docker
WORKDIR /app

# Copy the jar produced by Gradle into the container at /app
COPY build/libs/shop-0.0.1-SNAPSHOT.jar /app/app.jar

# Make port 8080 available to the world outside this container
EXPOSE 8080

# Run app.jar when the container launches
CMD ["java", "-jar", "/app/app.jar"]
