import de.imedia24.shop.controller.ProductController
import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductUpdateRequest
import de.imedia24.shop.service.ProductService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.springframework.http.HttpStatus
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import java.math.BigDecimal

class ProductControllerTest {

    @InjectMocks
    private lateinit var productController: ProductController

    @Mock
    private lateinit var productService: ProductService

    private val mockMvc: MockMvc

    init {
        MockitoAnnotations.initMocks(this)
        mockMvc = MockMvcBuilders.standaloneSetup(productController).build()
    }

    @Test
    fun testGetProductsBySkus() {
        val skuList = listOf("123", "456", "789")
        val product1 = ProductResponse("123", "Product 123", "Description 1", BigDecimal.valueOf(99.99), 10)
        val product2 = ProductResponse("456", "Product 456", "Description 2", BigDecimal.valueOf(49.99), 5)
        val product3 = ProductResponse("789", "Product 789", "Description 3", BigDecimal.valueOf(19.99), 20)

        val mockResponse = Pair(listOf(product1, product2, product3), "All products found")
        Mockito.`when`(productService.findProductsBySkus(skuList)).thenReturn(mockResponse)

        val responseEntity = productController.findProductsBySkus(skuList)

        val expectedResponse = mapOf(
            "foundProducts" to listOf(product1, product2, product3),
            "message" to "All products found"
        )

        Mockito.verify(productService, Mockito.times(1)).findProductsBySkus(skuList)
        Assertions.assertEquals(HttpStatus.OK, responseEntity.statusCode, "Expected HTTP status code to be OK")
        Assertions.assertEquals(expectedResponse, responseEntity.body, "Expected body to match the mock response")
    }
    @Test
    fun testGetProductsBySkusWithMissingSkus() {
        val skuList = listOf("123", "456", "890")  // Assuming SKU "890" is missing
        val product1 = ProductResponse("123", "Product 123", "Description 1", BigDecimal.valueOf(99.99), 10)
        val product2 = ProductResponse("456", "Product 456", "Description 2", BigDecimal.valueOf(49.99), 5)

        val mockResponse = Pair(listOf(product1, product2), "Products not found for SKUs: [890]")
        Mockito.`when`(productService.findProductsBySkus(skuList)).thenReturn(mockResponse)

        val responseEntity = productController.findProductsBySkus(skuList)

        val expectedResponse = mapOf(
            "foundProducts" to listOf(product1, product2),
            "message" to "Products not found for SKUs: [890]"
        )

        Mockito.verify(productService, Mockito.times(1)).findProductsBySkus(skuList)
        Assertions.assertEquals(HttpStatus.OK, responseEntity.statusCode, "Expected HTTP status code to be OK despite missing SKUs")
        Assertions.assertEquals(expectedResponse, responseEntity.body, "Expected body to match the mock response with missing SKUs")
    }

    @Test
    fun testAddProduct() {
        val productRequest = ProductRequest("999", "New Product", "New Description", BigDecimal.valueOf(29.99), 15)
        val createdProduct = ProductResponse("999", "New Product", "New Description", BigDecimal.valueOf(29.99), 15)

        Mockito.`when`(productService.createProduct(productRequest)).thenReturn(createdProduct)

        val responseEntity = productController.addProducts(productRequest)

        Mockito.verify(productService, Mockito.times(1)).createProduct(productRequest)
        assert(responseEntity.statusCode == HttpStatus.CREATED)
        assert(responseEntity.body == createdProduct)
    }


    @Test
    fun testUpdateProduct() {
        val sku = "123"
        val updateRequest = ProductUpdateRequest("Updated Product", "Updated Description", BigDecimal.valueOf(129.99))
        val updatedProduct = ProductResponse("123", "Updated Product", "Updated Description", BigDecimal.valueOf(129.99), 10)

        Mockito.`when`(productService.updateProduct(sku, updateRequest)).thenReturn(updatedProduct)

        val responseEntity = productController.updateProduct(sku, updateRequest)

        Mockito.verify(productService, Mockito.times(1)).updateProduct(sku, updateRequest)
        assert(responseEntity.statusCode == HttpStatus.OK)
        assert(responseEntity.body == updatedProduct)
    }

    @Test
    fun testUpdateProductNotFound() {
        val sku = "999"
        val updateRequest = ProductUpdateRequest("Updated Product", "Updated Description", BigDecimal.valueOf(129.99))

        Mockito.`when`(productService.updateProduct(sku, updateRequest)).thenReturn(null)

        val responseEntity = productController.updateProduct(sku, updateRequest)

        Mockito.verify(productService, Mockito.times(1)).updateProduct(sku, updateRequest)
        assert(responseEntity.statusCode == HttpStatus.NOT_FOUND)
    }

}
