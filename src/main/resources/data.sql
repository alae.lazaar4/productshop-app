-- Insert Product 1
INSERT INTO products (sku, name, description, price, created_at, updated_at) VALUES ('8901', 'WATCH', 'APPLE WATCH', 2765, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

-- Insert Product 2
INSERT INTO products (sku, name, description, price, created_at, updated_at) VALUES ('123', 'LAPTOP', 'HP core i7', 13765, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

-- Insert Product 3
INSERT INTO products (sku, name, description, price, created_at, updated_at) VALUES ('4567', 'MOUSE', 'MOUSE DELL', 365, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

-- Insert Product 4
INSERT INTO products (sku, name, description, price, created_at, updated_at) VALUES ('2345', 'SMART TV', 'SAMSUNG V8', 9765, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
