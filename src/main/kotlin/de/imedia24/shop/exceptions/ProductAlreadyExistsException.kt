package de.imedia24.shop.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.BAD_REQUEST)
class ProductAlreadyExistsException(message: String) : RuntimeException(message)
