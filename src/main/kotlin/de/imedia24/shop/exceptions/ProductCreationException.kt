package de.imedia24.shop.exceptions

class ProductCreationException (message: String, cause: Throwable) : RuntimeException(message)


