package de.imedia24.shop.controller

import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductUpdateRequest
import de.imedia24.shop.service.ProductService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.net.URI

@Api(value = "Product Management System")
@RestController
@RequestMapping("/products")
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @ApiOperation(value = "View a product by SKU")
    @GetMapping("/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(
        @PathVariable("sku") sku: String): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)
        return ResponseEntity.ok(product)
    }


    @ApiOperation(value = "View products by list of SKUs")
    @GetMapping("")
    fun findProductsBySkus(@RequestParam skus: List<String>): ResponseEntity<Map<String, Any>> {
        logger.info("Request for products by list of skus: $skus")

        val (products, message) = productService.findProductsBySkus(skus)
        val responseBody = HashMap<String, Any>()
        responseBody["foundProducts"] = products
        responseBody["message"] = message

        return ResponseEntity.ok(responseBody)
        }


    @ApiOperation(value = "Add new product")
    @PostMapping("/addProducts")
    fun addProducts(@RequestBody product: ProductRequest): ResponseEntity<ProductResponse> {

        val createdProduct = productService.createProduct(product)
        return ResponseEntity.created(URI.create("/product/${createdProduct.sku}")).body(createdProduct)
    }

    @ApiOperation(value = "Update an existing product by SKU")
    @PatchMapping("/update/{sku}")
    fun updateProduct(@PathVariable sku: String, @RequestBody request: ProductUpdateRequest): ResponseEntity<ProductResponse> {
        logger.info("Request for updating product with sku $sku ")

        val updatedProduct = productService.updateProduct(sku, request)
        return if (updatedProduct != null) ResponseEntity.ok(updatedProduct) else ResponseEntity.notFound().build()
    }

}