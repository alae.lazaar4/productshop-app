package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import de.imedia24.shop.domain.product.ProductUpdateRequest
import de.imedia24.shop.exceptions.ProductAlreadyExistsException
import de.imedia24.shop.exceptions.ProductCreationException
import de.imedia24.shop.exceptions.ProductNotFoundException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.ZonedDateTime

@Service
class ProductService(private val productRepository: ProductRepository) {

    @Transactional(readOnly = true)
    fun findProductBySku(sku: String): ProductResponse {
        val product = productRepository.findBySku(sku)
        return product?.toProductResponse()
            ?: throw ProductNotFoundException("Product not found for SKU: $sku")
    }

    fun findAllProducts():List<ProductResponse>{
        return productRepository.findAll().map { it.toProductResponse() }
    }

    @Transactional(readOnly = true)
    fun findProductsBySkus(skus: List<String>): Pair<List<ProductResponse>, String> {
        val products = productRepository.findAllById(skus).toList()
        val foundSkus = products.map { it.sku }
        val notFoundSkus = skus.filterNot { foundSkus.contains(it) }
        val message = if (notFoundSkus.isNotEmpty()) {
            "Products not found for SKUs: $notFoundSkus"
        } else {
            "All products found"
        }
        return Pair(products.map { it.toProductResponse() }, message)
    }

    @Transactional
    fun createProduct(request: ProductRequest): ProductResponse {
        val existingProduct = productRepository.findBySku(request.sku)
        if (existingProduct != null) {
            throw ProductAlreadyExistsException("Product with SKU ${request.sku} already exists maybe try update endpoint.")
        }

        try {
            val entity = ProductEntity(
                sku = request.sku,
                name = request.name,
                description = request.description,
                price = request.price,
                stock = request.stock,
                createdAt = ZonedDateTime.now(),
                updatedAt = ZonedDateTime.now()
            )
            val savedEntity = productRepository.save(entity)
            return savedEntity.toProductResponse()
        } catch (ex: Exception) {
            throw ProductCreationException("Error creating the product", ex)
        }
    }

    @Transactional
    fun updateProduct(sku: String, request: ProductUpdateRequest): ProductResponse? {
        val existingProduct = productRepository.findBySku(sku)
        existingProduct?.let {
            val updatedProduct = it.copy(
                name = request.name ?: it.name,
                description = request.description ?: it.description,
                price = request.price ?: it.price,
                updatedAt = ZonedDateTime.now()
            )
            return productRepository.save(updatedProduct).toProductResponse()
        }
        throw ProductNotFoundException("Product not found for SKU: $sku")
    }
}
