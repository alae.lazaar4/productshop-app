# Step 1: Install Docker
If not installed , download Docker Desktop for Windows from the official Docker website and install it. Ensure it's running by checking the Docker icon in the system tray.

# Step 2: Create a Dockerfile
Navigate to the root directory of your project. Create a file named Dockerfile (without any extensions) and open it in a text editor.

Check  the following content into the Dockerfile:


# FROM openjdk:8-jre-slim

 Set the working directory in docker
# WORKDIR /app

 Copy the jar produced by Gradle into the container at /app
# COPY build/libs/shop-app-0.0.1-SNAPSHOT.jar /app/app.jar

 Make port 8080 available outside of the container
# EXPOSE 8080


 Specify the command to run when the container starts
# CMD ["java", "-jar", "/app/app.jar"]

# Step 3: Build the Application JAR
Before creating a Docker image, you need to build your application. Navigate to your project's root directory in Command Prompt or PowerShell, then run:


gradlew clean build
This will compile your code, run tests, and produce a JAR file in the build/libs/ directory.

2. Build the Docker Image
 use the command
# docker build -t shop-app:latest .
This command builds a Docker image from your Dockerfile and names it imedia24-app with the tag latest.

3. Run the Docker Container
   Step 5: Run the Container
use the command 
# docker run -p 8080:8080 shop-app:latest
This command runs a container from the imedia24-app image, and maps port 8080 inside the container to port 8080 on your host machine.

Step 6: Access the Application
If your application is a web service listening on port 8080, you can now access it via a browser or any HTTP client by navigating to:


